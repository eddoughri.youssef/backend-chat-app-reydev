const jwt = require("jwt-then");

module.exports = async (req,res,next)=>{
    try{
        if(!req.headers.authorization) throw "Interdit1"
        const token = req.headers.authorization;
        const payload = await jwt.verify(token, process.env.SECRET);
        next();
    }catch(err){
        res.status(401).json({
            message: "Interdit"
        });
    }
};