
require("dotenv").config();
const mongoose = require('mongoose');
mongoose.connect(process.env.DATABASE, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

mongoose.connection.on("error", (err) => {
  console.log("Mongoose Connection ERROR: " + err.message);
});

mongoose.connection.once("open", () => {
  console.log("MongoDB Connected!");
});

//Bring in the models
require("./models/User");
require("./models/Chatroom");
require("./models/Message");

const app=require('./app');
const server=require("./websocket");

server.listen(3001, ()=>{
  console.log("WEBSOCKET: Server listening on 3001");
});

app.listen(3000, ()=>{
    console.log("APP: Server listening is on 3000");
});


