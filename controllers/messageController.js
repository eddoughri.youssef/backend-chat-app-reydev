const mongoose = require("mongoose");
const { Schema } = mongoose;


// const Chatroom = mongoose.model("Chatroom", Data);
const Message = mongoose.model("Message");
const Chatroom = mongoose.model("Chatroom");
const User = mongoose.model("User");


exports.loadMessage = async (userMessage, chatroom, userName)=>{
    if(userMessage === undefined) throw "Message is Empty";
    if(chatroom === undefined) throw "chatroom is undefined";
    if(userName === undefined) throw "user is undefined";

    const chatroomRequested = await Chatroom.findOne({ name: chatroom });
    const user = await User.findOne({ name: userName });
    const message = new Message({
        user,
        message: userMessage,
        chatroom: chatroomRequested
    });
    
    await message.save();

}

exports.createMessage = async (req, res) => {  
    const { userMessage, chatroom, userName } = req.body;
    
    await this.loadMessage(userMessage, chatroom, userName);
    
    res.json({
        message: "Message created!",
    });
};




exports.getAllMessages = async (req, res) => {
    const chatroom = req.params.chatroom;
    const cleaned = async function (msg) {
        const messages = [];
        for (let i = 0; i < msg.length; i++) {
            const user = await User.findOne({ _id: msg[i].user });
            messages.push( {
                user: user.name,
                message: msg[i].message
            });
        }
        return messages;
    }
    const chatroomRequested = await Chatroom.findOne({ name: chatroom });
    if (!chatroomRequested) throw "Chatroom with that name does not exists!";

    const messages = await Message.find({
        chatroom: chatroomRequested
    });
    const cleaned_msg = await cleaned(messages);
    res.json(cleaned_msg);
};
