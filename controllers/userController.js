const mongoose = require("mongoose");
const {Schema} = mongoose;
const sha256 = require("js-sha256");
const jwt = require("jwt-then");


const Data = new Schema({
  name: String,
  email: String,
  password: String
})

// const User = mongoose.model("User", Data); 
const User = mongoose.model("User");

exports.register = async (req, res) => {
    const { name, email, password } = req.body;
  
    const emailRegex = /@gmail.com|@yahoo.com|@hotmail.com/;
    if (!emailRegex.test(email)) throw "Email is not supported from your domain.";
    if (password.length < 6) throw "Password must be atleast 6 characters long.";
  
    const userExists = await User.findOne({
      email,
    });
  
    if (userExists) throw "User with same email already exits.";
  
    const user = new User({
      name,
      email,
      password: sha256(password + process.env.SALT),
    });
  
    await user.save();
  
    res.json({
      message: "User [" + name + "] registered successfully!",
    });
  };
  
exports.login = async (req, res) => {
  const { email, password } = req.body;
  // console.log(email, password)

  const user = await User.findOne({
    email,
    password: sha256(password + process.env.SALT),
  });

  if (!user) throw "Email and Password did not match.";

  const token = await jwt.sign({ id: user.id }, process.env.SECRET);

  res.json({
    message: "User logged in successfully!",
    token,
  });
};

exports.verify = async (req,res)=>{
  try{
      if(!req.headers.authorization) throw "Interdit1"
      const token = req.headers.authorization;
      const payload = await jwt.verify(token, process.env.SECRET);
      res.status(200).json({
        message: "Token is Verified"
      })
  }catch(err){
      res.status(401).json({
          message: "Interdit"
      });
  }
};

exports.profile = async (req, res)=>{
  const token = req.header('Authorization').replace('Bearer ', '');
  // Verify the JWT token
  try {
    const decoded = await jwt.verify(token, process.env.SECRET);
    const user = await User.findOne({
      _id: decoded.id
    });
    // Get the user information from the database
    // const user = await findUserById(decoded.userId);
    console.log(user)
  
    // Return the user information
    res.send({
      name: user.name,
      email: user.email
    });
  } catch (error) {
    res.status(401).send({ error: 'Invalid token' });
  }
}


