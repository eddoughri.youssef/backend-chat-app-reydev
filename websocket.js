const http = require('http');
const mongoose = require("mongoose")
const WebSocket = require('ws');
const server = http.createServer();
const ws = new WebSocket.Server({ server }); // complet the websocket implemntion in your local branch than merge and PUSH (^-^).
const jwt = require("jwt-then");
const User = mongoose.model("User");
const Message = require("./controllers/messageController");





ws.on("connection",async (socket, request)=>{
    // const token = request.headers.authorization; you cannot send headers using ws protocol !!! apply plan B branch "ticket"
    try{
        const chatroom = request.url.match(/chatroom=(.*)&/)[1];
        const token = request.url.match(/&token=(.*)/)[1];
        const payload = await jwt.verify(token, process.env.SECRET);
        if(!chatroom) throw "Need to specify the chatroom";
        const user = await User.findOne({ _id: payload.id });
        socket.userName = user.name;
        socket.chatroom = chatroom;
    }catch(err){
        socket.close();
        console.log("dead user", err);
        return;
    }
    console.log("user", socket.userName,"entred the room", socket.chatroom);
    socket.on("open", ()=>{
        if(!socket.userName){
            console.log("user undefiend");
            socket.close();
            return;
        }
    })

    socket.on("close", ()=>{
        console.log(socket.userName, "has close it connection");
        socket.close();
    });


    socket.on("message", (msg)=>{
        if(!socket.userName){
            console.log("user undefiend");
            socket.close();
            return;
        }
        console.log("user: ", socket.userName, "has send: ", msg);
        Message.loadMessage(msg, socket.chatroom, socket.userName);
        ws.clients.forEach((client)=>{
            if (client.readyState === WebSocket.OPEN) {
                client.send(msg);
            }
        });
    });
});



module.exports = server;